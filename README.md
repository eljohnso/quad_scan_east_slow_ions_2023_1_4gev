# quad_scan_east_slow_ions_2023_1_4GeV

Measurement done with the 1.4 GeV/u ion beam to the EAST Area

[Main repo: Quad Scan East](https://gitlab.cern.ch/eljohnso/quad-scan-east)